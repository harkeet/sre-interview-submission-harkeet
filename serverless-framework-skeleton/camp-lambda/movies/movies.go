package movies

import (
	// "encoding/json"
	"errors"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Movie struct {
	Year   int     `json:"Year"`
	Title  string  `json:"Title"`
	Plot   string  `json:"Plot"`
	Rating float64 `json:"Rating"`
}

/// newDynamodbClient creates a client with the default aws session
func newDynamodbClient() (*dynamodb.DynamoDB, error) {
	sess, err := session.NewSession()
	if err != nil {
		return nil, err
	}
	return dynamodb.New(sess), nil
}

const tableName = "CAmpSRETable"

// func AddMovie(req events.APIGatewayProxyRequest) (
// 	*Movie,
// 	error,
// ) {

// 	var item Movie

// 	dynaClient, err := newDynamodbClient()
// 	// Add Error handling for Movie Attributes

// 	av, err := dynamodbattribute.MarshalMap(item)
// 	if err != nil {
// 		return nil, errors.New("Got error marshalling new movie item:")
// 	}

// 	input := &dynamodb.PutItemInput{
// 		Item:      av,
// 		TableName: aws.String(tableName),
// 	}

// 	_, err = dynaClient.PutItem(input)
// 	if err != nil {
// 		return nil, errors.New("Got error calling PutItem:")
// 	}
// 	return &item, nil
// }

func GetMovie(req events.APIGatewayProxyRequest) (*Movie, error) {

	dynaClient, err := newDynamodbClient()
	payload := &dynamodb.GetItemInput{
		TableName: aws.String("CAmpSRETable"),
		//https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_AttributeValue.html
		Key: map[string]*dynamodb.AttributeValue{
			"Year": {
				N: aws.String(req.QueryStringParameters["Year"]),
			},
			"Title": {
				S: aws.String(req.QueryStringParameters["Title"]),
			},
		},
	}

	result, err := dynaClient.GetItem(payload)
	if err != nil {
		return nil, errors.New("Failed to Get Item")

	}

	// if result.Item == nil {
	// 	return nil, fmt.Errorf("Could not find '%v'", payload.Title)
	// }

	item := new(Movie)

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		return nil, fmt.Errorf("Failed to unmarshal Record, %v", err)
	}
	return item, nil

}
