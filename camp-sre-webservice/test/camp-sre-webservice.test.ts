import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as CampSreWebservice from '../lib/camp-sre-webservice-stack';

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new CampSreWebservice.CampSreWebserviceStack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
