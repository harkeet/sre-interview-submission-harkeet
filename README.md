**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## APPROACH 1 - CONVERTING THE GO CODE INTO SINGLE LAMBDA FUNCTION

Started with converting the Golang application as a simple web service hosted as lambda function with an API Gateway and DynamoDB backend.

Image

1. Modified the code to receive and send response as API Gateway Proxy request events.
2. Tried using the request path from the API Gateway Proxy request as routing to server different endpoints but faced issue in routing the requests with a third party router.
3. Tried embedding the AWS adapter and Gorilla MUX router which receives an API Gateway proxy event, transforms it into an http.Request object, and sends it to the mux router handle function for routing. This approch needed some time to get the code working.

Code can be found on <> this folder in repo.

## Shortcoming for Approach 1
As the application add additional routes the Lambda function becomes more complex and deployments of new versions would require to replace the entire function. With this approach it also becomes harder for multiple developers to work on the same project.

---

## APPROACH 2 – DECOUPLING GO CODE INTO MULTIPLE LAMBDA FUNCTIONS
Using the multiple single purpose lambda functions gives you more flexibility in terms of development and deployment as developers from different domains can work on separate endpoints logics and also it becomes easier to reuse the code template. Using a specific Lambda function to server a specific use case can also gives you can option to restrict permissions depending on the use case.

<image>

Solution is submitted using the above approach.

---
## PROPOSED ARCHITECTURE

<image>

AWS Service Used:

* [Lambda]
* API Gateway
* Cognito
* DynamoDB
* CloudWatch
---

## LAMBDA FUNCTIONS

Following are the 
* HomeHandler:
  * Endpoint: /
  * Request: ANY request
  * Input: Triggering the endpoint url
  * Output: Home Message
  * Authentication: Not set for root
* UpsertHandler:
  * Endpoint: /upsert
  * Request: POST request
  * Input: Client send the Movie json data structure.
  * Output: Item is added to DynamoDB table and message is returned.
  * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token. 
* GetItem:
  * Endpoint: /show
  * Request: GET request
  * Input: Client passes in the Movie Title and Year to be retrieved via endpoint.
  * Output: Movie data is outputted
  * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token.
* ErrorHandler:
  * Endpoint: /error
  * Request: GET request
  * Input: Triggering the error endpoint
  * Output: Error message
  * Authentication: Cognito Authentication configured and requires Authentication header in the request containing valid ID_Token.

--- 

### Choice of Infrastruce as Code (IaC)
Options:
1. Terraform
2. CDK (Typescript/Python)
3. Terraform for CDK (dumps into terraform json config instead of CF)
4. Serverless Framework (I used to setup a base sekeleton for testing the go code)

Choosed **CDK with TypeScript** language as I also wanted to take this exercide as a learning experience for Typescript.

--- 

### Prerequisites

To work with the AWS CDK, you must have an AWS account and credentials and have installed Node.js and the AWS CDK Toolkit. See AWS CDK Prerequisites.

You also need TypeScript itself. If you don't already have it, you can install it using npm.

```sh
$ npm install -g typescript
```

### CDK Installation
Install or update the [AWS CDK CLI] from npm (requires [Node.js ≥ 10.13.0](https://nodejs.org/download/release/latest-v10.x/)). We recommend using a version in [Active LTS](https://nodejs.org/en/about/releases/)
⚠️ versions `13.0.0` to `13.6.0` are not supported due to compatibility issues with our dependencies.

```console
$ npm i -g aws-cdk
```

## Useful commands

 * `cdk deploy`  deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk synth`       emits the synthesized CloudFormation template

## Clone the github repo

```console
$ git clone git-url.git
$ cd sre-interview-submission-harkeet/camp-sre-webservice
```

To synthesize a CDK app, use the cdk synth command. Let’s check out the template synthesized from the sample app:
cdk synth

Managing AWS Construct Library modules
Use the Node Package Manager (npm), included with Node.js, to install and update AWS Construct Library modules for use by the stack, as well as other packages you need. (You may use yarn instead of npm if you prefer.) npm also installs the dependencies for those modules automatically.

The AWS CDK core module is named @aws-cdk/core. AWS Construct Library modules are named like @aws-cdk/SERVICE-NAME. The service name has an a prefix.

Use the following command to install required modules

```console
$ npm install @aws-cdk/core @aws-cdk/aws-lambda @aws-cdk/aws-dynamodb @aws-cdk/aws-apigateway @aws-cdk/aws-cognito @aws-cdk/aws-cloudwatch
```
Project's dependencies are maintained in package.json.
```npm
npm update
```

Deploy this to your account:

```console
$ cdk deploy
```

Use the `cdk` command-line toolkit to interact with your project:

 * `cdk deploy`: deploys your app into an AWS account
 * `cdk synth`: synthesizes an AWS CloudFormation template for your app
 * `cdk diff`: compares your app with the deployed stack


